# Convert a markdown file to a Microsoft Word doc. Good for
# handouts.

docker run --rm -v `pwd`:/data pandoc/core:latest $1 -o $2