# Convert a markdown file to a latex-generated pdf, for when you need
# to be a Serious Academic Person.

docker run --rm -v `pwd`:/data pandoc/latex:latest -s $1 -o $2 --pdf-engine=xelatex