# Assignments

- [Week 0: Getting Ready](/assignments/week00/)
- [Week 1: The Internet](/assignments/week01/)
- [Week 2: The Application Layer: HTTP](/assignments/week02/)
- [Week 3: The Application Layer: DNS and Other Protocols](/assignments/week03/)
