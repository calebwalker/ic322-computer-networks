# Week 7: Transport Layer Security

## Learning Goals

- I can explain how two strangers are able to exchange secret keys in a public medium.
- I can walk through the TLS handshake and explain why each step is necessary.
- I can explain how TLS prevents man-in-the-middle attacks.

## Readings and Resources

* *Kurose*, Section 8.1 - 8.6 (pay special attention to section 8.6)

## Eat Your Vegetables

Eat your vegetables using the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).

This week's questions are from Chapter 8.
* From the Review Questions: R20, R21, R22, R23
* From the Problems section: P9

## Grow More Vegetables

Contribute some self-assessment questions to the class's question bank. This can include writing new questions or editing/improving existing ones. ([Our Class Bulletin Board has the login information.](https://docs.google.com/document/d/1M0ZK6oFZVgWBMS4ZgqY_FCec8o2WSLQ7LBkrCxC2IXE/edit?usp=sharing))

## Community Chest

Do at least one thing to improve the physical or digital space of the class.

## Labs

Choose **one** and add it to your Portfolio using the [Lab standards](/standards/README.md#lab-assignments).

* **Explore TLS with Wireshark**. Complete the textbook TLS Wireshark Lab [on this website](https://gaia.cs.umass.edu/kurose_ross/wireshark.php). Make sure to use the 8.1 version. Submit the answers in your Portfolio.
* Complete a "Deep Dive" in accordance with the [Deep Dive standards](/standards/README.md#deep-dives).

## 📓 Portfolio Submission

This week your Portfolio should include:

1. A Lab writeup of your choice, in accordance with the [Lab Standards](/standards/README.md#lab-assignments) or [Deep Dive Standards](/standards/README.md#deep-dives).
2. Answers to the textbook questions and Learning Goals, in accordance with the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).
