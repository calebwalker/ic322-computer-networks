# Week 4: Transport Layer: TCP and UDP

## Learning Goals

* I can explain how TCP and UDP multiplex messages between processes using sockets.
* I can explain the difference between TCP and UDP, including the services they provide and scenarios each is better suited to.
* I can explain how and why the following mechanisms are used and which are used in TCP): sequence numbers, duplicate ACKs, timers, pipelining, go-back-N, selective repeat, sliding window.
* I can explain how these mechanisms are used to solve dropped packets, out-of-order packets, corrupted/dropped acknowledgements, and duplicate packets/acknowledgements.

## Readings and Resources

* *Kurose*, Sections 3.1 - 3.5.4
* [Slides and video lectures](https://drive.google.com/drive/folders/1jL6af03f8MtfakPr1LQKDMb74KUpzN5B)

## Eat Your Vegetables

Eat your vegetables using the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).

This week's questions are from Chapter 3. This week, focus on formatting your submissions correctly in markdown, and writing in complete sentences. Look at your Portfolio in Gitlab. Does it look how you expect? You can also use the Markdown Preview feature in VSCode (`Ctrl-Shift-P`, then type `Markdown: Open Preview`).

* From the Review Questions: R5, R7, R8, R9, R10, R11, R15
* From the Problems section: P3

## Grow More Vegetables

Contribute some self-assessment questions to the class's question bank. This can include writing new questions or editing/improving existing ones. ([Our Class Bulletin Board has the login information.](https://docs.google.com/document/d/1M0ZK6oFZVgWBMS4ZgqY_FCec8o2WSLQ7LBkrCxC2IXE/edit?usp=sharing))

## Community Chest

Do at least one thing to improve the physical or digital space of the class.

## Labs

Choose **one** and add it to your Portfolio using the [Lab standards](/standards/README.md#lab-assignments).

* **Explore TCP with Wireshark**. Complete the textbook TCP Wireshark Lab [on this website](https://gaia.cs.umass.edu/kurose_ross/wireshark.php). Submit the answers in your Portfolio.
* **[Introduction to Packet Tracer](/assignments/week04/lab-packet-tracer/)**.
* Complete a "Deep Dive" in accordance with the [Deep Dive standards](/standards/README.md#deep-dives).
<!-- * https://gaia.cs.umass.edu/kurose_ross/programming/RDT/RDT_Implementing%20a%20Reliable%20Transport%20Protocol.html -->

## 📓 Portfolio Submission

This week your Portfolio should include:

1. A Lab writeup of your choice, in accordance with the [Lab Standards](/standards/README.md#lab-assignments) or [Deep Dive Standards](/standards/README.md#deep-dives).
2. Answers to the textbook questions and Learning Goals, in accordance with the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).
