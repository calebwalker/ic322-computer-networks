# Introduction to Packet Tracer

In this lab, we have three goals: successfully run Cisco's network configuration simulation software *Packet Tracer*, learn Cisco's IOS in order to configure commercial network devices inside of Packet Tracer, and finally test our configured network using the layer-3 protocol ICMP — the Internet Control Message Protocol. ICMP is intimately involved with the network layer; in fact, it's given the Internet Protocol number 1.

This lab, like most of our labs, asks you to answer questions we haven't covered in class yet. This might be uncomfortable at first. You can discuss with your neighbors, use the internet, or - even better - use the textbook to find answers. Do your best!

ICMP exists at the "top" of layer-3 (that is, it's not a transport layer protocol and has no notion of ports, the layer-4 identifier), but it's also not strictly a layer-3 protocol in that it doesn't assist in routing, provide an addressing scheme, and is transported in the data portion of an IP datagram. A cursory Google search will turn up many examples of individuals with strong opinions about ICMP's proper place in the TCP/IP stack. Your instructors, along with most others, accept it as a "layer 3.5" protocol.

ICMP is defined in RFC 792, which was published in September, 1981. ICMP messages are sent in several circumstances — most students are familiar with the network utility "ping", which sends a type of ICMP message to a distant host or router. That distant host or router will respond with another type of ICMP message, which we'll see in lab today. Further, ICMP messages are used to report some errors in processing datagrams. For example, if a router can no longer forward a datagram to the next hop because the IP Time-To-Live (TTL) value has reached 0, it will reply to the sending host with a specific ICMP message informing the sender of the problem. 

## Deliverables

* Submit a writeup in accordance with [Lab Standards](/standards/README.md#lab-assignments). I recommend including your `pkt` file in your Portfolio in case you need to refer to it later.

## Resources

* [Cisco Commands PDF](/assignments/week04/lab-packet-tracer/cisco_commands.pdf)

## Step 1: Create a Cisco Account

Packet Tracer is installed on all the lab machines. However, in order to use the software you must have an account. (If you'd like to install Packet Tracer on your personal laptop it's freely available from Cisco and you're allowed to do so, but the installation instructions are outside the scope of these instructions.)


-   Sign-up for the *Introduction to Packet Tracer* course and a Cisco
    Networking Academy Account:
    -   Go to [Sign-Up Page](https://www.netacad.com/courses/packet-tracer/introduction-packet-tracer)
    - Scroll down to "Getting Started with Cisco Packet Tracer". Click the "View Course" link. You'll be redirected to a "Skills for All" page.
    - Once there, click "Get Started". This will prompt you to sign in to a "Skills for All" account.
    - Click "Sign up" and follow the instructions to create a new account.
    - Once you create your account, remember your email and password! You'll need them to use Packet Tracer on the lab machines.


## Configure Packet Tracer

Packer Tracer is installed on the lab machines. Go ahead and open it now.

There are some **configuration changes you must make** to avoid
headaches in this and future Packet Tracer Labs. In the top-menu
bar, click `Options`$\rightarrow$`Preferences`:

On the `Interface` tab it is recommended you match your settings
        to these:

![](figs/pt_preferences.png)

On the `Miscellaneous` tab it is recommended you check the `Buffer Filtered Events Only` box to allow Packet Tracer to behave smoothly when capturing network traffic:

![](figs/buffer_settings.png)

## Introduction to Packet Tracer

You may be asking yourself, "What in the world is Packet Tracer? And why
do I have to install another piece of software on my laptop..." To
answer that question: Packet Tracer is a *free* and useful way to
practice configuring network equipment and learning the Cisco CLI based
operating system without having to spend thousands of dollars on
*physical* networking equipment. It offers both GUI based configuration
for end devices and command line based configuration for routers and
switches using the *original* IOS, the Cisco Internetwork Operating
System. Let's layout what Packet Tracer simulates:

-   Physical configuration practice for hosts and packet switches:
    -   Cabling
    -   Module installation (Interfaces for routers and switches)
-   GUI based software configuration for servers (web, DNS, DHCP, etc.)
    and edge devices (laptops, VoIP phones, WiFi routers)
-   Command Line Interface (CLI) configuration of core network devices
    (routers and switches)
-   Common types of network traffic to enable detailed packet inspection

### Packet Tracer Tutorial Videos

If you do not enjoy clicking around in a program to figure out how it
works (admittedly Packet Tracer is not the friendliest of user
interfaces) there are included video tutorials that Cisco provides with
Packet Tracer and also from YouTube users. These are completely
optional, but taking the time to view them will get you comfortable with
using Packet Tracer in about one hour, which is a main goal of this lab.
You will use Packet Tracer for one more lab in the course and if you
ever desire to get network-related Cisco certifications like the CCNA
Routing and Switching Certification and others. To access the in-house
tutorials, in the top-menu click `Help`$\rightarrow$`Tutorials`. Here is
a list of recommended videos:

-   Interface overview
-   Creating a Network Topology
-   Configuring Devices Using the CLI Tab
-   Configuring Devices Using the Config Tab
-   Configuring Devices Using the CLI Tab
-   Simulation Environment
-   Simulation Panel
-   Advanced Simulation Mode
-   PDU Information

### Your first Packet Tracer Network and Simulation Mode

Alright, enough talking about the software, let's dive in by creating a
simple Local Area Network and completing a tutorial from Cisco that will
get you familiar with the simulation mode of Packet Tracer. Since this
is an intro lab, we will provided detailed steps to get you started.
Future labs will rely on your understanding of these steps and will not
provide every step required to complete a task. Make sure you ask
questions now as they arise! Complete the following steps to configure a
LAN consisting of a Link-Layer Switch and two hosts:

1.  Add your hosts. In the lower left hand corner, click `End Devices`
    category and then add a `PC` and `Laptop` to your workspace by
    dragging and dropping them:

![](figs/add_hosts.png)

2.  Add your switch. Click the `Network Devices` category and then the
    `Switch` sub-category. Add a 2950T switch to your workspace by
    dragging and dropping it:

![](figs/add_switch.png)

3.  Cable your hosts to the switch. For most cabling cases it is
    recommended you use the `Automatically Choose Connection Type`
    connector to let Packet Tracer pick the correct cable type for you.
    Sometimes we need cross-over cables, sometimes we need straight
    through cables, see
    [here](https://www.linkedin.com/pulse/difference-between-straight-through-crossover-cable-cheer-chen)
    if you are curious. Click the `Connections` category and the select
    the `Automatically Choose Connection Type` connector. Then click on
    a host and then the switch to connect them to the switch. Notice the
    interface labels automatically appear if you modified settings as
    required above. This will be helpful when we go to configure devices
    in the future.

![](figs/cabling_hosts.png)

4.  Assign IP address to *each* host. Use IPs in the `11.11.11.0/24`
    network, remembering the network and broadcast addresses are
    reserved! Also, it is a best practice to reserve the first
    assignable IP in a LAN for the Default Gateway. In other words, **do
    not** use `11.11.11.1` for a host, save it for when you connect a
    router to the switch later!

![](figs/config_host_ip.png)

Congratulations, you configured your first Packet Tracer network! Since
our switch is self-learning, it does not require any configuration past
cabling on our part! Now let's test our LAN using the *ICMP* protocol we
just learned about in class by using the `ping` utility.

5.  Click on one of your hosts, then click the `Desktop` tab and then
    the `Command Prompt` (Yes\... it's a "Windows" host :-) icon to open
    a terminal.

![](figs/command_prompt.png)

6.  Ping the other host using the IP you set. If you are successful you
    should see something like this:

![](figs/pt_first_ping.png)

Once you have successfully pinged between your hosts, you are ready to
learn how to observe traffic flowing across your network. Let's get
started.

7.  Start simulation mode by clicking the `Simulation` button in the
    lower right corner of Packet Tracer. Then click the `Show All/None`
    button in order to turn off all types of traffic being captured in
    simulation mode (all are selected by default and this is always too
    much traffic to sort through in any meaningful way). Finally, click
    the `Edit Filters` button in order to select only the types of
    traffic you want to select:

![](figs/simulation_button.png)

8.  Right now, we only care about `ARP` and `ICMP`. Select both of these
    protocols, ensuring no other types of traffic are selected
    (**NOTE:** There are multiple tabs to select protocol types):

![](figs/pt_filter_list.png)

9.  Go back to one of your hosts and ping the other host on your
    network. Be sure to click play in the simulation panel if nothing
    happens. You should see ICMP and ARP messages flowing between your
    two hosts. Observe until you see the completion of your four ICMP
    messages (the four requests/replies you see in your terminal). You
    should see something like this in your `Simulation Panel` indicating
    success:

![](figs/sim_panel_post_ping.png)

10.  Click on any of the packets in your `Simulation Panel` in order to inspect further on the information contained in the packet. Specifically, notice that Cisco uses that pesky 7-layer OSI model we mentioned when we introduced our 5-layer TCP-IP model. Click on the `Inbound PDU Details` tab in order to actually read the bytes of the packet separated by layer.
11.  When you are finished go back to Realtime mode by clicking the `Realtime` button next to the `Simulation` button.
12.  Inside packet tracer, type `Ctrl + S` and pick a location to save your first network so you can close packet tracer and come back to your network as necessary. Save the file as `Section_Lname_Fname.pkt` for submission at end of lab!

Congratulations, you have completed the Packet Tracer introduction
portion of this lab. While there were no questions to answer, you will
use the skills learned here again in this lab and in a future lab. Let's
get ready for the big leagues, no more configuring familiar hosts and
relying on link-layer switches to auto-learn information needed for
forwarding. You are ready to start configuring the workhorse of the
internet, routers!

## Cisco IOS Introduction

Now that you are familiar with Packet Tracer, we can move onto our
actual lab for today while introducing the Cisco IOS via the CLI. You
will be building out this simple network and then sending ICMP messages
(via `ping`) to different hosts in order to observe how ICMP helps us
troubleshoot network connectivity issues.

![](figs/complete_nw.png)

1.  First you have to add a router to your workspace. You can find the
    routers as depicted below, in this course you can always use the
    `2811` router:

![](figs/add_router.png)

2.  Now that we have the start of our network, it's time to get down to business...CLI business that is. Take a look at the [Cisco Commands PDF](cisco_commands.pdf). In the lab and elsewhere in the course, $<$alligators$>$ mean replace the example value in the alligators with your value or option!) It attempts to explain the four levels of Cisco IOS configuration modes and also provide the most prominent commands available at each level to get you started. Study it for a minute and try some of the commands in packet tracer. Click on the router you just added, then click on the *CLI* tab.
3.  Let's start configuring. Get to the `CLI` of the *2811 Router* (**NOTE:** If you get prompted to enter setup mode or the initial configuration dialog in any router or switch always answer "n"!). Set the router name to `LAN_Router`, starting from `Router >` here are the commands:

-   `en` (Shorthand for `enable`. You can use this shorthand with
    any command, as long as you type enough letters for it to match
    a unique command. Thanks Cisco!)
-   `conf t` (Shorthand for `configure terminal`)
-   `hostname LAN_Router`
-   You should instantly know your changes took place, see that
    `Router >` changed to `LAN_Router >` in your terminal.

4.  Now that our router has a proper name, let's cable it to our switch and then give it an IP address and subnet mask on our LAN. Remember that `11.11.11.1` IP I told you to reserve? Let's use it for our router interface that will serve as the `Default Gateway` for our hosts.

-   Run a cable as done previously (use the lightning bolt!) from
    the switch to the router.
-   In the `LAN_Router` terminal, from where you left off after
    re-naming the router, you should see\
    `LAN_Router(config)#`. If you do not see this checkout the Cisco
    Command Cheatsheet for help getting back. To get to the
    `LAN_Router (config-if)#` level for `Fa0/0` (or whatever
    interface your newly connected cable went to) run this command:
    `interface FastEthernet 0/0` (tab complete is your friend!)
-   Set the IP address of the interface:
    `ip address 11.11.11.1 255.255.255.0`
-   Run the command `no shut` (no shutdown) to turn on the interface

5.  Once you run `no shut` you should see the link turn green in the
    network view and also see the text below appear in the router's CLI.
    A link that is up/up refers to a connection that is administratively
    up (no shutdown) and the IP protocol is talking so it is configured
    correctly:

![](figs/up_up.png)

6.  Let's check your work and verify connectivity to the `Laptop`.

-   `Ctrl + z` (get back to the privileged exec mode)
-   `show ip route` (you should see `11.11.11.0` is directly
    connected)
-   `ping 11.11.11.X` (X, being the IP you chose for your laptop.
    You should get success rate greater than 50, ping again if low.)

7.  From privileged exec mode (you should be here), run the command: `show run` to view all the current configuration settings on the router. Hit the `spacebar` to scroll through. Verify you did not fat-finger any addressing information! You should see this:

![](figs/show_run.png)

8.  If you passed all the above tests, time to write your router's
    running configuration changes (currently only in the router's RAM)
    to the routers start-up configuration (long term memory). Do so by
    running: `copy run start`. **NOTE:** Hit `enter` at the prompt to
    accept the default save location! **WARNING:** If you don't run this
    command after making router changes they may be lost if you reboot
    the router for some reason in packet tracer. Always do this after a
    configuration change is successful!
9.  Go back to each host to configure your now working Default Gateway
    for the LAN. Go to the `Config` tab then `GLOBAL: Settings` and
    input the router's interface IP in the `Gateway` field. Since we
    haven't setup a Dynamic Host Configuration Protocol (DHCP) server
    yet, we have to provide all of this addressing information manually!
10.  Time to add a second LAN to our network to make our network match
    the diagram above! Repeat previous steps to add a second switch,
    with at least one host connected and configured, to the right side
    of our `LAN_Router`. Use the network `12.12.12.0/24` for this
    network, keeping in mind the actual assignable addresses and the
    best practice for the IP for the default gateway. Generally, ensure
    you complete these steps again:
    -   Add the host (I chose to add a server) and switch to your
        workspace.
    -   Cable all devices and connect the switch to the `LAN_Router`.
    -   Configure an IP for the new networks `LAN_Router` interface in
        the `12.12.12.0/24` Network, do not forget to run: `no shut` and
        `copy run start`!
    -   Configure an IP and the default gateway for your new host.

If all was configured successfully, you should now be able to ping a
host in your original `11.11.11.0/24` LAN from your new `12.12.12.0/24`
LAN. We added a Network Layer device, so now we are able to route
traffic from host to host across networks! If you can ping across LANs,
you have completed the configuration portion of the lab. Save the file
as `Section_Lname_Fname.pkt` for submission and then answer the
questions in the next section by observing ICMP traffic going across
your network.

## ICMP Introduction

Finally we are to the part of the lab where we can just sit back and
watch our network do work! Let's explore the usefulness of ICMP by
sending some pings across our packet tracer network and diving into what
information is contained in the ICMP messages based on the success or
failure of our pings.

Ping Success!
-------------

As we did in the Packet Tracer Introduction section, enter Simulation
Mode and ensure that you are only capturing `ARP` and `ICMP` traffic. Go
to a host from LAN 1 (`11.11.11.0/24` NW) and ping the second host in
LAN 1. Click play if necessary, and watch the traffic move through your
network that was generated by your ping. You may see ARP traffic occur
before your ICMP traffic, for review make sure you understand why, and
then answer the following questions about your ICMP traffic. Click play
again to pause the simulation after all traffic completes.

-   Open the first ICMP PDU at the top of the simulation panel and go to
    the `Outbound PDU Details` tab.


> **Question 1:** Find the `PRO:` field on the third row of the IP header. What is this
field and what is the number associated for our ICMP traffic? What does
this say about ICMP?



> **Question 2:** Scrolling down and looking into the ICMP Payload, what are the Type and
Code values for this first ICMP PDU? What do these values mean?
:


> **Question 3:** Find the third PDU listed for the first group of color-matched ICMP
messages. The last device should be the switch and the at device should
be the host you pinged. This is the hosts response to your ping! Looking
at the `Outbound PDU Details` tab, what are the Type and Code values for
this ICMP PDU? What do these values mean?


IP identifiers are used by network layer devices to
identify fragments that belong to the same original datagram before
fragmentation occurred, which aids the receiving device in reassembling
those fragments. Some ICMP messages have an identifier field too, called
the "ICMP ID" field. The ICMP ID serves a different purpose. Echo
Requests and Replies are two ICMP types that contain the ICMP ID field,
so let's take a look at this field next:


> **Question 4:** What are the ICMP ID values for each of the Echo Request messages? These are the first PDU in each of the ICMP color groupings (should be four).



> **Question 5:** What are the ICMP ID values for the Echo Response messages?  These are the last PDU in each of the ICMP color groupings (should be four).




> **Question 6:** What are the ICMP Sequence Number values for the Echo Request messages?



> **Question 7:** What are the ICMP Sequence Number values for the Echo Response messages?



> **Question 8:** What can you deduce about these ICMP messages based on your answers to
the previous four questions? Consider they were all generated from
running the `ping` command once, what are these numbers capturing?


Ping failures!
--------------

-   Click the reset simulation button in the simulation panel, go to a
    host in the 11.11.11.11.0/24 network and attempt to ping 8.8.8.8.
    Once traffic settles, open the third red ICMP PDU from the LAN
    Router as it arrives back at the device from which you attempted the
    ping and answer the following questions:


> **Question 9:** Which device crafted this ICMP traffic? What is the Type and Error Code?
Does this make sense?



> **Question 10:** Why does this ICMP response PDU have two IP headers and two ICMP
headers?


-   Click the reset simulation button in the simulation panel, go to a
    host in the 11.11.11.11.0/24 network and attempt to ping an IP you
    have yet to setup yet in the 12.12.12.0/24 network (i.e
    12.12.12.211). We want a failed ping here!


> **Question 11:** Did your host ever receive an ICMP reply or error message? Why or why
not?


-   Click the reset simulation button in the simulation panel, the click
    the `add complex PDU` button on the third bar down from the top (see
    image). Then fill out the form as follows, adjusting IPs where
    necessary to work in your network. The goal is to set the TTL to 1
    and try and ping a host that is two hops away:

![](figs/complex_pdu.png)


> **Question 12:** Looking at the first red ICMP packet from the router, what is the type
(convert from hex to decimal) and code contained in the first listed
ICMP header? Is this expected? Is the reason for this error message
evident in the second IP header (the one from our original ping)
contained in the ICMP error datagram?


Be sure to submit your configured network saved in a Packet Tracer
(`Section_Lname_Fname.pkt`) file in Google Classroom!
