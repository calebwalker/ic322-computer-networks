# Developing Our Class Rubric

There are three goals for all students in IC322, in order of priority:

1. Understand the design and significance of the important concepts in computer networks.
2. Enjoy learning about networks.
3. Learn how to learn on your own, and how to evaluate your own progress.

Today we will lay the groundwork for goal 3. At six, twelve, and final weeks you will be evaluating your own portfolio of work and recommending a grade for yourself. To make our jobs easier, we will develop a "class rubric" to use when evaluating your work.

<div style="page-break-after: always;"></div>

## Defining Learning

**Individually, answer the following questions. After a few minutes you will discuss your answers in your small groups.**

In which class (during high school of college) do you think you learned the most/deepest? Why?
  
<br><br><br><br><br>

What's the difference between an academic research course and a hobby? Professors and researchers routinely publish papers that aren't graded. Why?

<br><br><br><br><br>

What do you define "learning" as? Or, in other words, if this class's goal is for students to "learn about computer networks", what would that look like to you in the best case scenario?

<br><br><br><br><br>

<div style="page-break-after: always;"></div>

## Evaluating Individual Assignments

**Individually, follow the directions below. After a few minutes you will discuss your answers in your small groups.**

This course will have many assignments. Look at the [course standards](/standards/) and read about each of them. 

What attributes would you use to evaluate an assignment?
* Correctness?
* Completeness?
* Effort?
* Prettiness/handwriting? Code comments or style? Grammar?
* "Growth"? (As in, a student has learned a *lot* rather than arriving in the course already knowing.)
* Bravery? (Trying to achieve something ambitious, but falling short.)
* What else?
List the attributes you think are important, and assign them relative weightings. (Such as, correctness is 50%, prettiness is 10%, etc.)

<br><br><br><br><br>

<div style="page-break-after: always;"></div>

## Evaluating Overall Class Performance

**Individually, answer this question. After a few minutes you will discuss your answers in your small groups.**

When students are given final course grades, what would you want to be taken into account?

* Quality of written assignments?
* Performance on Knowledge Checks?
* Turning in assignments on time?
* What about missing work?
* Reading the textbook?
* Doing extra work, such as completing two labs for a single assignment?
* Impact on classmates, such as teaching or helping other students?
* Contributions to the class, such as correcting and adding to course material?
* Class attendance?
* What else?

List the attributes you think are important to indicate learning, and assign them relative weightings.

<br><br><br><br><br>

<div style="page-break-after: always;"></div>

## Reducing it all down to a single letter

During our assessment weeks we will spend *a lot* of time reviewing our past work and evaluating ourselves based these rubrics. We will hopefully have a broad and deep understanding of how we are learning. Perhaps you understand everything you've completed, but you're a week behind. Maybe you understand half of every week's Learning Goals. There are lots of different ways to be.

But eventually it will come time to assign a single overall letter grade. We will talk about this a lot more during coming weeks, and especially during the six week assessment period.
