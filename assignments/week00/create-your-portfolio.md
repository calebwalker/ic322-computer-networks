# Creating a New Portfolio for IC322

This lab will guide you through creating your portfolio. At the end, you will have a repository available on Gitlab, and a nice welcome message for visitors. You will be doing a lot of your work this semester with `git` and Markdown. The [References section](/references/) has more in-depth guides on those specific topics. You should check them out!

> **August 21 Update**: I added a section to the [Git Reference](/references/git-reference.md) about using Git For Windows instead of WSL. If you're having connection issues, you might try using Git for Windows. I also added a section on SSH keys. If you're having trouble pushing your code to Gitlab, try setting up SSH keys.

## Create a Gitlab account

You don't *have* to use Gitlab, use can use Github if you prefer. But Gitlab is open source and USNA has its own locally-hosted Gitlab server, so this guide will focus on using that platform.

You also can use [GitLab.com](http://gitlab.com/) rather than the USNA-hosted Gitlab. USNA's GitLab server is only accessible from within the intranet (or using a VPN), so using the Gitlab.com hosted server might be more convenient. [GitLab.com](http://gitlab.com/) hosting is currently free, although there's no guarantee it will remain free forever. In any case, the majority of this guide will work on both the USNA-hosted GitLab server as well as the [GitLab.com](http://gitlab.com/) hosted service. It's also very easy to switch later if you change your mind.

1. To start using [gitlab.usna.edu](gitlab.usna.edu), you will need to register. To do so, click the "Register now" link.

2. Fill out the form, use your USNA email, and click Register.

That should be it! Now you can log in.

## Create an empty repository on Gitlab

You will often hear the term "repository". This is a version-controlled folder full of code. In a way, it acts as a "package" of code. You can "clone a repository" with a single command, when you update a remote server, you update all the changes you made in an entire repository. But unlike software packages, a repository usually contains source code, not compiled or runnable code.

A "project" in Gitlab is basically a repository. Gitlab calls it something different because Gitlab offers a lot of services on top of hosting your source code, but for this step what we are doing is creating a new project.

1. Create a new project by clicking the blue "New Project" button at the top right of the Gitlab website.

<figure>
<img src="assignments/week00/gitlab-dashboard.png">
<figcaption>This should be what you see first after logging in. You probably won't have any projects the first time you log in.</figcaption>
</figure>

2. After clicking "New project", select "Create blank project".

<figure>
<img src="assignments/week00/gitlab-new-project.png">
</figure>

3. Set up your project:
    * Enter a project name. ("IC322 Portfolio" would make sense)
    * Select a "group or namespace" if it's blank. You probably only have one option: your username. 
    * Set your project to "Public" so that other people (like your instructor) can see your Portfolio and give you feedback.
    * Deselect "Initialize repository with a README".

<figure>
<img src="assignments/week00/gitlab-blank-project.png">
</figure>

4. Click "Create project" and you're done! Well, you're done creating the empty online repository.

<figure>
<img src="assignments/week00/gitlab-project-created.png">
<figcaption>This is what an empty online repository looks like.</figcaption>
</figure>

## Create some code on your computer

1. Create a new folder on your computer. This folder will contain all the "code" for your portfolio.

2. In that folder, use your favorite text editor to create a file called `README.md`. In this file, write a heading and a bit of content:

```markdown
# LCDR Downs IC322 Portfolio

I'm maintaining this Portfolio for IC322: Computer Networks. If you're visiting, welcome! Feel free to leave feedback by opening an issue or submitting a merge request.
```

3. Save the file.


## Create a local repository (and a short primer on `git`)

We've created a project/repository on Gitlab, but we haven't yet talked about the command-line tool called `git`. `git` is the program that Gitlab, Github, and many other online services are built on.

`git` works like this:

You're working on a software project on your computer. You want to maintain a history of every version of your code in case you make a "breaking" change and need to revert to a previous version. This is called "version control", and is one of the services `git` provides. You can navigate to the folder that contains your code and run the following command:

```
git init
```

`git` will respond:

```
Initialized empty Git repository in <folder>
```

You can now call this local folder a "repository". What the `git` tool did (behind the scene) was create a new folder called `.git/`. Since the folder name starts with a period, it's considered a hidden folder and a normal `ls` command won't show it. But you can use `ls -a` to show all files, including hidden files and folders.

```
$ ls -a
.  ..  .git  README.md
```

The repository is still considered "empty" because we haven't saved any versions of our code to the version control system. In `git`, saving versions of our code is called "committing". Snapshots, or past versions of our code, we call "commits". We'll do this in the next step.

A few weeks and dozens of commits later, you team up with a classmate. You want to collaborate on the same software package. If you've uploaded ("pushed") your repository to Gitlab or Github (which we will do later), your classmate can "clone" your repository on their computer. Your classmate can make changes, commit them, and push the changes back to the server. You can "pull" the changes onto your computer. This system was built so that thousands of strangers could work together to create Linux, one of the most successful software projects in history.

1. Go ahead and run the `git init` command in your Portfolio folder if you haven't already.


## Commit the code in your local repository

Once you've made changes or added files to your local repository, you need to "commit" these changes. This means you're saving a version of your code to the local repository.

1. First, let's see what changes `git` has detected. Type:

```
git status
```

You should see `README.md` under "Untracked files".

2. Add the file to be committed:

```
git add README.md
```

3. Now, commit the file with a message:

```
git commit -m "Added initial README.md for my portfolio"
```

The -m flag allows you to add a message to your commit, which is a brief description of what you changed.

## Link your local repository to your (empty) Gitlab repository

Now that you have both a local and a remote repository, it's time to link the two. This way, you can "push" (or upload) your code from your local computer to the GitLab server.

1. First, navigate back to your GitLab project page. It should have a URL like https://gitlab.usna.edu/yourusername/yourprojectname, but hopefully you still have it open in your browser.

2. On this page, look for a "Clone" button. Click it, and you should see a dropdown with a URL. This is the address of your remote repository. Copy the URL under "Clone with HTTPS" if you're like to type in your username and password everytime you cant to push changes to the server. If you'd like to set up SSH keys, you can use the SSH URL and [set up SSH keys in Gitlab](/references/git-reference.md#setting-up-ssh-keys).

<figure>
<img src="assignments/week00/gitlab-clone-button.png" width="200">
</figure>

3. Now, in your terminal (make sure you're in your local repository's directory aka your Portfolio folder on your computer), type:

```
git remote add origin YOUR_COPIED_URL
```

`git` won't respond if the operation is successful, so don't worry if nothing prints out.

This command tells `git` to set the remote repository's nickname to "origin" and provides the address of the repository. You can name it anything, but it's conventional to call it `origin`.

## Push Your Changes from Your Local Repository to the Remote Repository

Now that you've committed your changes locally and added a remote server to your local repository, it's time to push your code to your GitLab repository.

1. Simply type:

```
git push origin main
```

You might get an error: 
```
error: failed to push some refs to 'https://gitlab.usna.edu/downs/ic322-portfolio.git'
```

If this is the case, `git` is using the legacy naming scheme and has called your primary branch `master` instead of `main`. Nbd, you can push your changes with this command:

```
git push origin master
```

If everything was successful, you can refresh your project page on Gitlab and see your uploaded code:

<figure>
<img src="assignments/week00/gitlab-pushed-project.png">
</figure>

Notice that the `README.md` file you created is automatically rendered on your project "homepage". We will take advantage of Gitlab's rendering of markdown files to build our Portfolio every week.

> **Troubleshooting**: If you get authentication errors when pushing your code, try [setting up SSH keys](/references/git-reference.md#setting-up-ssh-keys) and using the SSH URL instead of the HTTPS URL.

## Making more changes to your Portfolio

The process for updating a `git` repository (or, "repo", as we say in the biz) is:

1. Make changes to the code on your computer
2. Add those changes to the `git` "staging area"
3. Commit those changes to `git`
4. Push the changes to the remote server.

If we've already made changes to our code, steps 2-4 look like:
```

$ git add .

$ git commit -m "Added portfolio creation tutorial"
[main 1222693] Added portfolio creation tutorial
 11 files changed, 385 insertions(+), 7 deletions(-)
 create mode 100644 assignments/week00/create-your-portfolio.md
 create mode 100644 assignments/week00/gitlab-blank-project.png
 create mode 100644 assignments/week00/gitlab-clone-button.png
 create mode 100644 assignments/week00/gitlab-dashboard.png
 create mode 100644 assignments/week00/gitlab-new-project.png
 create mode 100644 assignments/week00/gitlab-project-created.png
 create mode 100644 assignments/week00/gitlab-pushed-project.png

$ git push origin main
Enumerating objects: 26, done.
Counting objects: 100% (26/26), done.
Delta compression using up to 12 threads
Compressing objects: 100% (16/16), done.
Writing objects: 100% (17/17), 1.48 MiB | 23.99 MiB/s, done.
Total 17 (delta 3), reused 0 (delta 0), pack-reused 0
To gitlab.com:jldowns-usna/ic322-computer-networks.git
   c11fde7..1222693  main -> main
```

## Using Visual Studio Code
You don't have to use the command line! VSCode, for example has a `git` sidebar:

<figure>
<img src="assignments/week00/vscode-git-sidebar.png" width="200">
</figure>

You can add all the updated files to the staging area, set the commit message, and commit the changes to the version control system with a single button click (the orange "Commit" button.)

After that, if you've configured your remotes already, you can push another button to push the changes to the server:

<figure>
<img src="assignments/week00/vscode-git-sync.png" width="200">
</figure>

Now, VSCode is using its own terminology. Clicking "Sync Changes" does more than just push local changes to the server. It also pulls changes that others have made and pushed to the server (or changes you made on a different computer and pushed.) This is fine and I'd recommend it.

There are other `git` interfaces besides the command line, but I won't cover them here.

## Report your Portfolio URL

You're done creating your Portfolio! Now you just need to let your instructor know where they can find it. You can do that by logging it in our [Portfolio URL sheet](https://docs.google.com/spreadsheets/d/1SbdaI0zwxYHdQhYcpV1SnTV6ytNiuI11Pp2pc9M8dDg/edit#gid=0), located in our [Community Materials drive](https://drive.google.com/drive/folders/1OsXHeg7SPHC1k5SgDYtfE9fzGsmNwlB7).

Check out the [References section](/references/) for more information about `git` and Markdown!

