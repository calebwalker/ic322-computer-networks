# An Internet Warmup

In this class we will be learning about all the different pieces of the internet and how they come together to create the biggest communications network in the world. You probably have *some* idea of how the pieces fit together, even if you only have a vague idea. Let's warm up by putting those ideas on paper. Don't worry if you're wrong! This is a warmup.

Collaboratively, in your small groups, create a diagram of the Internet. Try to use as many of these vocab words as you can.

- Hosts / end systems
- Clients
- Servers
- ISPs
- CDN
- IP Address
- MAC Address
- Subnet
- Router
- Switch
- Port

Afterward, we will share our diagrams with the class.