# Week 6: Six Week Review

During the first six weeks of this class we laid the foundations of 

- How the Internet works, how ISPs connect together, and how our computers and systems connect to the Internet
- How applications talk to each other using HTTP.
- We learned how to query API endpoints in order to run programs on other computers and receive their output.
- We learned what domain names are and how our computers use them to address our data.
- We know how email works! To a degree. But we do know how it's different than webpages (HTTP). We might even use email's example to build our own custom protocols.
- We now know what the Transport Layer does and how TCP creates a reliable data channel in a lonely, unreliable world.


# Week 1: The Internet

1. Open your Portfolio to your Week 1 Learning Objectives. Read your answers. Has your idea of encapsulation become more clear since you gave your answer here?

2. Open your Portfolio to your Week 1 Chapter Questions. Review your answers. Do you think you put more effort into these answers than in later weeks? Less? About the same? Why do you think that is?

3. Review your Week 1 Lab. How have your Markdown skills evolved over the past 6 weeks?



