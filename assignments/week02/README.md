# Week 2: The Application Layer

## Learning Goals 

* I can explain the HTTP message format, including the common fields.
* I can explain the difference between HTTP GET and POST requests and why you would use each.
* I can explain what CONDITIONAL GET requests are, what they look like, and what problem they solve.
* I can explain what an HTTP response code is, how they are used, and I can list common codes and their meanings.
* I can explain what cookies are used for and how they are implemented.
* I can explain the significance HTTP pipelining.
* I can explain the significance of CDNs, and how they work.

## Readings and Resources

* *Kurose*, Sections 2.1, 2.2, and 2.7

## Eat Your Vegetables

Eat your vegetables using the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).

This week's questions are from Chapter 2: R5, R8, R11, R12, R13, R14, and R26.

## Grow More Vegetables

Contribute some self-assessment questions to the class's question bank. This can include writing new questions or editing/improving existing ones.

### Community Chest

Do at least one thing to make the digital or physical space of the classroom better for everyone.

## Labs

This week you have a choice of labs. Choose **one** and add it to your Portfolio using the [Lab standards](/standards/README.md#lab-assignments).

- **Explore HTTP with Wireshark**. Complete the textbook HTTP Wireshark Lab [on this website](https://gaia.cs.umass.edu/kurose_ross/wireshark.php). Submit the answers in your Portfolio.
- **[Build a Simple Server](/assignments/week02/lab-build-a-server.md)**.
- **[Build a multiplayer `telnet` game](/assignments/week02/lab-telnet-game.md)**.
- Complete a "Deep Dive" in accordance with the [Deep Dive standards](/standards/README.md#deep-dives).

## 📓 Portfolio Submission

This week your Portfolio should include:

1. A Lab writeup of your choice, in accordance with the [Lab Standards](/standards/README.md#lab-assignments) or [Deep Dive Standards](/standards/README.md#deep-dives).
2. Answers to the textbook questions and Learning Goals, in accordance with the [Eat Your Vegetables standards](/standards/README.md#eat-your-vegetables).