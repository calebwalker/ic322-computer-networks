# Create a Multiplayer Telnet Game

We saw in class that `netcat` can be used to make TCP connections with a server. Most servers we talked to spoke HTTP. However, HTTP is only one of many layer 5 protocols.

In ye olden days people created the first networked games by sending all the game input and output directly through a TCP connection. The games didn't use HTTP to talk with a game client that translated the data into graphic or game events (like games do today). Instead, they simply sent the text they wanted to display.

The linux `telnet` program was commonly used to play these games. However, `telnet` is actually its own protocol, not just a pure TCP connection, and the `telnet` program will respond to `telnet` commands. This causes problems in some edge cases. `netcat` was designed to be a pure TCP client with no special handling at the application layer. 

> **🚨 Be Aware:** Of all the labs this week, this one gives you the least amount of help and the most freedom. Before choosing this one, decide if that's what you want. 

## Step 1: Create a Working Network Program

Read Chapter 2.7 and build a working version of the TCP client and server found in section 2.7.2. The server can serve as the basis for your telnet game.

## Step 2: Make your server support multiple concurrent connections

One problem with the textbook code is that `serverSocket.accept()` is [blocking](https://www.geeksforgeeks.org/blocking-and-nonblocking-io-in-operating-system/). If we were to turn this code into a chat program (that broadcast all messages received to all other clients), the server would hang waiting for a new connection before checking sockets for incoming messages.

The easiest solution is to start a new thread for each incoming connection. Here is an example chat program that uses threads to handle incoming connections: [`telnet-chat-example.py`](https://gitlab.com/jldowns-usna/ic322-computer-networks/-/blob/main/assignments/week02/telnet-chat-example.py)

## Step 3: Make the game!

Now you have everything you need to support multiple connections to your server. Now you just need to make a fun game!

> **💡 Tip:** I've found that "turn based" games like bowling and chess are more difficult than "asynchronous games" like traditional RPG MUDs. Turn based games require keeping track of whose turn it is, each client needs to wait until its turn, and then when its turn does come it asks the user for input. In asynchronous games the client can accept input at any time, and the server can send output at any time.

Here are a few game students made last year, for inspiration:
* [Chess Server](https://gitlab.com/jldowns-usna/chess_server), *Midn Ron Miller*
* [Telnet Bowling](https://gitlab.com/jldowns-usna/telnet-bowling), *Midn Anna-Grace Dumas & Midn Matthew Lewis*


## Portfolio Submission

Include this activity in your Portfolio in accordance with the [Lab Standards](/standards/README.md#lab-assignments). This lab does not have any included questions. It is expected that your *Process* section is especially robust and explains obstacles you faced, how you overcame them, and other interesting lessons from your development.