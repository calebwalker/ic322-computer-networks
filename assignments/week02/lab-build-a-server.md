# Build a Server

In section 2.7.2 of our *Computer Networking* textbook, the author goes over how to program a very simple TCP client and server. In this lab, we want to create a more sophisticated server: one that is able to communicate with a standard browser.


## Step 1: Review

Read Chapter 2.7 and build a working version of the TCP client and server found in section 2.7.2.

> **Question 1:** Why are we focusing on the TCP server in this lab rather than the UDP server?

## Step 2: Make a Valid HTTP Server

The goal of this lab is to create a server that will respond to an HTTP request from a browser, and return a valid HTTP response.

1. Adjust your server to print out a request from the browser. Instead of responding with an uppercase version of the request, print out the request on the command line.

2. Navigate to `http://localhost:12000/index.html` in your browser and observe the message that is sent to your server. Compare this to the HTTP Request Message format in the textbook section 2.2.3.

3. Study the HTTP Response format in the textbook (a few paragraphs after the Request Message format) and read the explanation. Edit your server to send a valid HTTP response with a simple HTML message.

4. Try navigating again to `http://localhost:12000/index.html:`. 

> **Question 2:** Look carefully at Figure 2.9 (General format of an HTTP response message). Notice that there's a blank line between the header section and the body. And notice that the blank line is two characters: `cr` and `lf`. What are these characters and how do we represent them in our Python response string?

**Tips:**
* Keep in mind the difference between HTTP and HTML. HTTP is the protocol that the browser and your server are using to communicate. It has a header section and a body and is explained in section 2.2.3. The body of the HTTP response should contain HTML, which is a language used to make webpages. An example of HTML is "`<html><body>Hello, world!</body></html>`"

* `socket.send()` does not support a standard String as an argument. You must encode the string into a bytes encoding first. For example:
  ```
  response = "Hello, world!"
  connectionSocket.send(response.encode())
  ```

* If you're stuck trying to build a valid HTTP response, try this:
  ```
  response = "HTTP/1.1 200 OK\r\nHello!"
  ```

* While starting and restarting your server, you may get an error:
  ```
  Traceback (most recent call last):
    File "/private/tmp/gof/serve.py", line 4, in <module>
      serverSocket.bind(('',serverPort))
  OSError: [Errno 48] Address already in use
  ```  
  This error indicates that your program is trying to connect to a socket that is already taken by another process. In this case, that process is a previous run your program. Your program didn't inform the operating system that it no longer needed the socket before it terminated, so the operating system is still reserving the resources for that process.  

  The quick solution is to choose a different port number when that happens. After a few minutes the OS will free up the socket and you can go back to using the previous port number if you like.

## Step 3: Make a File Server

You already have a working server! The problem is that to change the webpage you have to edit your server's source code. It's more common for simple servers to respond with the contents of files. That way you can store all your HTML in a file and all your server logic in the server code.

The textbook website has some skeleton code to help you complete this step. It's not required that you use their code, but [you can access it here](https://gaia.cs.umass.edu/kurose_ross/programming/Python_code_only/WebServer_programming_lab_only.pdf) if you'd like.

The basic steps are:
1. Parse the filename from the HTTP request.
2. Use Python to read the file and convert it to a string.
3. Create a response string like you did in the previous step. Except, instead of using hardcoded HTML use the file contents.

> 💡 Don't forget to `encode()` the strings you send over the network!

> **Question 3:** When a client requests the "`/index.html`" file, where on your computer will your server look for that file?

Ensure that your server is capable of serving multiple files. Here are two files you can use to test this:

```html
<!-- index.html -->

<html>
<head>
    <!-- Link to the external JavaScript file -->
    <script src="script.js" defer></script>
</head>
<body>
    <h1>Hello!</h1>
    Welcome to my website!
    <div>
        <h2>Counter: <span id="counterDisplay">0</span></h2>
        <button onclick="increaseCounter()">Increase</button>
        <button onclick="decreaseCounter()">Decrease</button>
        <button onclick="resetCounter()">Reset</button>
    </div>

</body>
</html>
```

```js
//script.js

let counter = 0;

function increaseCounter() {
    counter++;
    document.getElementById('counterDisplay').textContent = counter;
}

function decreaseCounter() {
    counter--;
    document.getElementById('counterDisplay').textContent = counter;
}

function resetCounter() {
    counter = 0;
    document.getElementById('counterDisplay').textContent = counter;
}
```

## Step 4: Support Images

You have a pretty good webserver so far! However, websites are much better with images. It's not a huge leap from the current text-only webserver to one that supports non-text files like images. But there are a few considerations:

1. You have to actually set the `Content-type` value in the response header.
2. You have to read non-text files in binary mode.

You have Options to complete this step:

* 🧠 Research and develop image support with raw brainpower.
* 🤖 Take this opportunity to practice coding with an AI. You can use Copilot or a Chatbot to modify your code to support images.

Be sure to test your code and include the code and a writeup in your Portfolio Submission.

## Portfolio Submission

Include this activity in your Portfolio in accordance with the [Lab Standards](/standards/README.md#lab-assignments)