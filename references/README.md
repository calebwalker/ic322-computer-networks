# IC322 References

* [Markdown Reference](/references/markdown-reference/)
* [Python Quick-start](/references/python-reference.md)
* [Git for Students Who Need to Get Work Done](/references/git-reference.md)
* [IC322 Community Google Drive](https://drive.google.com/drive/folders/1OsXHeg7SPHC1k5SgDYtfE9fzGsmNwlB7)
* [Sample Portfolio](https://gitlab.com/jldowns-usna/ic322-downs-portfolio)
