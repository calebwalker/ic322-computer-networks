# Markdown

Markdown is a plaintext file format designed to be both machine- and human-readable. It's common for people to write their "content" in Markdown and let a server render it in HTML. Github, Gitlab, and most websites support Markdown natively.

This site is written in Markdown. [Check out the source code for this file](https://gitlab.com/jldowns-usna/ic322-computer-networks/-/blob/main/references/markdown-reference/README.md?plain=1).

## Creating a Markdown file

By convention, markdown files end in a `.md` extension.

## Organizing Files and Folders

Gitlab will automatically display the `README.md` file in each folder. Gitlab will also automatically match relative links in markdown to folders in your repository. For example, if you have a folder called `standards` with a `README.md` file in it, you can link to it using `/standards/`.

Take a look at [the course website's repository](https://gitlab.com/jldowns-usna/ic322-computer-networks). Look at the list of links at the bottom and click the "Assignment Standards" link.

Now go back to the repository root (by clicking your browser's back button) and click the "standards" folder in the code listing. You should arrive at the same spot!

Compare that to the folder/file structure I used in [my sample Portfolio](https://gitlab.com/jldowns-usna/ic322-downs-portfolio).

Markdown will also render markdown files *not* called README.md; you just have to link to them explicitly. For example, you can link to the [Markdown Reference](/references/markdown-reference/) using `/references/markdown-reference/`. The [Reference section of this website](https://gitlab.com/jldowns-usna/ic322-computer-networks/-/tree/main/references?ref_type=heads) uses that organization.

Deciding which organization you prefer is a personal choice. I prefer to use a `README.md` file in folders when I will have a lot of files in that folder, or when I want to keep different topics separated. For example, each week gets its own folder on the course website. Right now I link to individual files in the Reference section since they're generally related (they're all references) and I didn't expect each guide to require many files. But, as I've been writing them, I've found that I want to include images and other files. I might move each guide into its own folder soon.

More specifics about relative and absolute links are covered [in a later section of this guide](#links).

## Common Styling

You can add italics by surrounding words with astericks:

> TCP doesn't offer speed guarantees, but it *does* offer reliable data transfer.
>
> `TCP doesn't offer speed guarantees, but it *does* offer reliable data transfer.`

Bold is two asterisks:

> **Problem 2:** Which layer provides a host-to-host logical connection?
>
> `**Problem 2:** Which layer provides a host-to-host logical connection?`

You can format code inline using backticks:

> The `rwnd` variable keeps track of the number of bytes that the receiver is willing to accept.
>
> ``The `rwnd` variable keeps track of the number of bytes that the receiver is willing to accept.``

Code blocks are created with three backticks. You can enable syntax highlighting by adding the language name after the first set of backticks:

> ```python
> while True:
>     # Wait for a client to attempt a connection, and accept it.
>     client_object, client_address = s.accept()
>     print(f"Connection from {client_address}!")
>     client_message = client_object.recv(1024)
>     print(client_message)
>     client_object.close()
> ```
>
>     ```python
>     while True:
>         # Wait for a client to attempt a connection, and accept it.
>         client_object, client_address = s.accept()
>         print(f"Connection from {client_address}!")
>         client_message = client_object.recv(1024)
>         print(client_message)
>         client_object.close()
>     ```

Greater than signs are used to create block quotes (which I've been using to format the examples in this document):

> > The Internet is a network of networks.
>
> `> The Internet is a network of networks.`

(yep, you can nest block quotes)

## Headings

Headings are created by prefixing a line with one or more `#` characters. The number of `#` characters determines the heading level:

> # Heading 1
>
> `# Heading 1`
>
> ## Heading 2
>
> `## Heading 2`
>
> ### Heading 3
>
> `### Heading 3`
>
> #### Heading 4
>
> `#### Heading 4`

For more readable but more difficult to type headings, you can underline headings with `=` or `-` characters:

> Heading 1
> =========
>
> ```
> Heading 1
> =========
> ```
>
> Heading 2
> ---------
>
> ```
> Heading 2
> ---------
> ```

## Links

Links are build using a bracket-parenthesis pair:

> [RFC 1918](https://datatracker.ietf.org/doc/html/rfc1918) defines the private IP address ranges.
>
> `[RFC 1918](https://datatracker.ietf.org/doc/html/rfc1918) defines the private IP address ranges.`

You can create links to other files in the same repository by using relative paths:

> Make sure the assignments in your Portfolio are submitted according to the [course standards](/standards/README.md).
>
> `Make sure the assignments in your Portfolio are submitted according to the [course standards](/standards/README.md).`

If you provide a reference to a folder, the link will default to the `README.md` file in that folder:

> Make sure the assignments in your Portfolio are submitted according to the [course standards](/standards/).
>
> `Make sure the assignments in your Portfolio are submitted according to the [course standards](/standards/).`

You can also link directly to headings in files:

> Labs should be entered in your Portfolio using the [Lab standards](/standards/README.md#lab-assignments).
>
> `Labs should be entered in your Portfolio using the [Lab standards](/standards/README.md#lab-assignments).`

## Lists

You can create ordered lists by starting each line with a number followed by a period:

> 1. Application
> 2. Transport
> 3. Network
> 4. Data Link
> 5. Physical
>
> ```
> 1. Application
> 2. Transport
> 3. Network
> 4. Data Link
> 5. Physical
> ```

You don't have to start with the correct number. Markdown will automatically number the list for you. This makes the list less human-readable but make reordering items a little easier (or much easier for large lists).

> 1. Application
> 1. Transport
> 1. Network
> 1. Data Link
> 1. Physical
>
> ```
> 1. Application
> 1. Transport
> 1. Network
> 1. Data Link
> 1. Physical
> ```

You can create unordered lists by starting each line with a dash (`-`) or an asterisk (`*`):

> - Application
> - Transport
> - Network
>
> * Data Link
> * Physical
>
> ```
> - Application
> - Transport
> - Network
>
> * Data Link
> * Physical
> ```

You can nest lists by indenting the nested list:

> 1. Application
>    - HTTP
>    - FTP
>    - SMTP
> 1. Transport
>    1. TCP
>    1. UDP
>
> * Circuit-switched networks
>   1. PSTN
>   1. ISDN
> * Packet-switched networks
>   - X.25
>   - Frame Relay
>
> ```
> 1. Application
>    - HTTP
>    - FTP
>    - SMTP
> 1. Transport
>    1. TCP
>    1. UDP
>
> * Circuit-switched networks
>   1. PSTN
>   1. ISDN
> * Packet-switched networks
>   - X.25
>   - Frame Relay
> ```


## Images

Images are similar to links, but they start with an exclamation point. The text in the brackets is used as the alt text for the image:

> On Gitlab you can toggle between code and rendered views of markdown files using this button:
>
> ![Markdown Toggle](<md-toggle.png>)
>
> ```
> On Gitlab you can toggle between code and rendered views of markdown files using this button:
> 
> ![Markdown Toggle](<md-toggle.png>)
> ```


While it's better to store images in the same repository as the markdown file that uses them, you can also link to images on the Internet:

> ![ISS](https://images-assets.nasa.gov/image/KSC-20150810-PH-NAS01_0022/KSC-20150810-PH-NAS01_0022~small.jpg)
>
> `![ISS](https://images-assets.nasa.gov/image/KSC-20150810-PH-NAS01_0022/KSC-20150810-PH-NAS01_0022~small.jpg)`

Markdown doesn't give a lot of options for formatting images but you can always fall back to html:

> <figure>
> <img src="https://images-assets.nasa.gov/image/KSC-20150810-PH-NAS01_0022/KSC-20150810-PH-NAS01_0022~small.jpg" alt="ISS" width="200">
> <figcaption>The ISS has a 600MB internet connection.
> </figure>
> 
> ```
> <figure>
>       <img src="https://images-assets.nasa.gov/image/KSC-20150810-PH-NAS01_0022/KSC-20150810-PH-NAS01_0022~small.jpg" alt="ISS" width="200">
>       <figcaption>The ISS has a 600MB internet connection.
> </figure>
> ```

## Tables

You can create a table by separating columns with pipes (`|`) and using a row of dashes (`-`) to separate the header from the body:

> | Layer | Protocol         | Packet Name |
> | ----- | ---------------- | ----------- |
> | 5     | Application      | Message     |
> | 4     | Transport        | Segment     |
> | 3     | Network          | Datagram    |
> | 2     | Data Link        | Frame       |
> | 1     | Physical         | Bit         |
>
> ```
> | Layer | Protocol         | Packet Name |
> | ----- | ---------------- | ----------- |
> | 5     | Application      | Message     |
> | 4     | Transport        | Segment     |
> | 3     | Network          | Datagram    |
> | 2     | Data Link        | Frame       |
> | 1     | Physical         | Bit         |
> ```

You can use colons in the header row to align columns:

> | Product Name | Price   | In Stock  |
> | ------------ | ------: | :-------: |
> | Apples       | 1.99    |     Yes   |
> | Oranges      | 2.99    |     Yes   |
> | iPads        | 415.99  |     No    |
>
> ```
> | Product Name | Price   | In Stock  |
> | ------------ | ------: | :-------: |
> | Apples       | 1.99    |     Yes   |
> | Oranges      | 2.99    |     Yes   |
> | iPads        | 415.99  |     No    |
> ```


## Math

Gitlab supports Math equations.. sort of. For files named `README.md` you can use KaTeX syntax to render nice equations.

> $$\sum_{i=1}^{n} i = \frac{n(n+1)}{2}$$
>
> ```
> $$\sum_{i=1}^{n} i = \frac{n(n+1)}{2}$$
> ```

You can find more examples at the [Gitlab documentation](https://docs.gitlab.com/ee/user/markdown.html#math).

However, for files *not* named `README.md`, ***Gitlab will only render equations in the first 1000 characters of your document*** ([see this issue](https://gitlab.com/gitlab-org/gitlab/-/issues/333514).) Our assignments quickly surpass 1000 characters in a few paragraphs, so if you want to use pretty math equations, you should use the "README-in-a-folder" [method of file organization](#organizing-files-and-folders).



