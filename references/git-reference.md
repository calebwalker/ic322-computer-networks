# Git for Students Who Need to Get Work Done

## Setting up `git` for Windows

`git` is included on Linux and MacOS machines, but not Windows. Windows does come with WSL, which allows you to run a Linux environment on your Windows machine, but sometimes there are problems connecting to the internet on the mission network. If you have problems with WSL, you can use a Windows version of `git` called [Git for Windows](https://gitforwindows.org/).

First, follow the instructions to [install Git for Windows](https://gitforwindows.org/). You can use the default settings for the installation.

After it is installed, you need to configure VSCode to use the `git` that was installed with Git for Windows. Open VSCode and go to File ➜ Preferences ➜ Settings. Search for "git path" and change the setting to the path where you installed Git for Windows. For example, if you installed it in the default location, you would change the setting to `C:\Program Files\Git\bin\git.exe`.

## Cloning a Repository

To start "working on" an existing project (that is being version controlled with `git`), you need to make a copy of the code on your local machine. This is called "cloning" the repository.

To clone a repository, you need to know the URL of the repository. Github and Gitlab have colorful buttons for the clone URL.

<figure>
<img src="references/github-clone-button.png" width="300">
<img src="references/gitlab-clone-button.png" width="300">
<figcaption>Github and Gitlab clone URLs</figcaption>
</figure>

Once you have the URL, you can clone the repository with the `git clone` command. The following command will clone this repository to your computer:

```bash
git clone https://gitlab.com/jldowns-usna/ic322-computer-networks.git
```

Now that the repository is cloned, you can access the code even if you're not connected to the internet. You can also make changes to the code and commit them to your local repository.

## Updating Your Local Repository

When you clone a repository, you get a snapshot of the code at that point in time. If the repository is updated, you will need to update your local repository to get the latest changes.

To update your local repository, you can use the `git pull` command. This will pull down any changes that have been made to the repository since you cloned it.

```bash
git pull origin main # or master, depending on your branch name
```

## Committing Changes

When you make changes to the code, you need to commit them to your local repository. This is a two-step process. First, you need to add the files you want to commit to the staging area. Then, you need to commit the files in the staging area to the repository.

To add files to the staging area, you can use the `git add` command. For example, if you want to add all the files in the current directory to the staging area, you can run:

```bash
git add .
```

To commit the files in the staging area to the repository, you can use the `git commit` command. You need to include a message with your commit. The message should be a short description of the changes you made. For example:

```bash
git commit -m "Added a new function to the code"
```

## Pushing Changes

Once you have committed your changes to your local repository, you can push them to the remote repository. This will make your changes available to everyone else who has access to the remote repository.

To push your changes, you can use the `git push` command. For example:

```bash
git push origin main # or master, depending on your branch name
```

## Setting up SSH Keys

If you don't want to type in your username and password everytime you push changes to the remote repository, you can set up SSH keys. This uses public key cryptography, rather than a password, to authenticate you to the remote repository. In this scheme, you have a file on your computer (your key) that you can use to log in.

First, you need a key. Your generate this on your computer. The following command generates a key and saves it to the file `~/.ssh/id_ed25519`:

```bash
ssh-keygen -t ed25519 -C "user@email.com"
```

You can use the default settings for the key. You can also set a passphrase for the key, but this is optional. If you set a passphrase, you will need to enter it every time you use the key.

Next, you need to add the key to your Gitlab account. You can do this by copying the contents of the file `~/.ssh/id_ed25519.pub` (`.pub` indicates "public key") and pasting it into the appropriate place in your account settings:
1. Click on your profile picture in the upper right corner of the screen
2. Click on "Preferences"
3. Click on "SSH Keys"
4. Select "Add new key"
5. Copy and paste your public key into the "Key" field.
6. Click "Add key"

<figure>
<img src="references/gitlab-ssh-keys.png" width="300">
<img src="references/gitlab-ssh-keys-2.png" width="200">
<img src="references/gitlab-ssh-keys-3.png" width="300">
<img src="references/gitlab-ssh-keys-4.png" width="500">
</figure>

Finally, you need to tell your computer to use the key when connecting to the remote repository. To do this, you need to add the key to your SSH agent. You can do this with the following command:

```bash
ssh-add ~/.ssh/id_ed25519
```

You also need to make sure you set up the remote URL to use SSH. If you already set up the remote URL to use HTTPS, you can change it with the following command:

```bash
git remote set-url origin <ssh-url>
```

If you *haven't* set up the remote URL yet, you can do it with the following command:

```bash
git remote add origin <ssh-url>
```

Keep in mind that this key resides on a specific computer. If you want to push changes from a different computer, you will need to generate a new key on *that* computer and add it to your Gitlab account.


## Doing all this in Visual Studio Code

VS Code comes with `git` functions builtin. [They have a great guide](https://code.visualstudio.com/docs/sourcecontrol/overview)! But here are the highlights:

**Clone a repository** by following the directions earlier in this guide. For example:

```
➜ git clone https://gitlab.com/jldowns-usna/ic322-computer-networks.git
Cloning into 'ic322-computer-networks'...
remote: Enumerating objects: 234, done.
remote: Counting objects: 100% (155/155), done.
remote: Compressing objects: 100% (142/142), done.
remote: Total 234 (delta 70), reused 0 (delta 0), pack-reused 79
Receiving objects: 100% (234/234), 1.61 MiB | 9.55 MiB/s, done.
Resolving deltas: 100% (94/94), done.
```

This created a directory on my computer called `~/ic322-computer-networks`.

**Open the folder** in VS Code through the menu File ➜ Open Folder. Select the folder `git` just created for you (for me, it's `~/ic322-computer-networks`). If it asks you to "trust the authors", decide whether or not you trust me and then make your choice.

On the left, there is a Version Control tab. **Click it**.

<figure>
<img src="references/vscode-version-control-tab.png" width="300">
</figure>

<figure>
<img src="references/vscode-version-control-nothing.png" width="300">
</figure>

Right now there's nothing to do since your local copy is up to date with the server. But I'll be making changes to this course website on a regular basis. It's a good idea to fetch changes from the server regularly. You can press the little "sync" button in the status bar to check for changes.

<figure>
<img src="references/vscode-sync-button.png" width="200">
</figure>

Or... VS Code can check for changes (autofetch) automatically, although it's disabled by default. To turn on autofetch, visit the settings (Code ➜ Settings) and search for "autofetch". Change the value to `all` to enable it for all repositories.

<figure>
<img src="references/vscode-autofetch.png" width="400">
   </figure>

Now when there is a change on the server, VS Code will alert you to sync the changes in the Version Control Tab.

<figure>
<img src="references/vscode-change-detected.png" width="300">
</figure>


When you update your own portfolio, you'll need to add files to the staging area, commit them, and push them to the server. This is simple in VS Code.

You'll find all the files you changed in the Version Control tab

<figure>
<img src="references/vscode-changed-files.png" width="300">
</figure>

In this image, none of my files are "staged" for committing yet. You can either click the "+" sign to stage the files you want to commit or let VS Code add them all for you. First, add a commit message. Then click the "Commit" button.

<figure>
<img src="references/vscode-with-commit-message.png" width="300">
</figure>

This creates a "commit", or a checkpoint, on your local computer. But the changes are still only on your local machine, not on the remote server. To sync your changes to the server, click the "Sync Changes" button.

<figure>
<img src="references/vscode-ready-to-sync.png" width="300">
</figure>

After that, your changes are in sync with the server!

This might have felt like a lot of steps, but after some practice it becomes second nature. Most importantly, this is a basic skill for computer programmers in the 21st century. `git` isn't perfect, and one day it may be replaced, but right now there is nothing even close to replacing it.


## Troubleshooting

### Changes I push to the server aren't showing up.

One problem might be that you have both a `main` and a `master` branch. This can happen if you forget to uncheck the "add README.md file to project" when you set up your project. To fix this:

1. **Figure out which branch your work is on.** On your computer, the command `git status` will tell you which branch you're on.
2. **Set that branch to be the default branch.** On the server, go to Settings ➜ Repository ➜ Default Branch. Change the default branch to the branch you're working on. Now when you view your repository, you should see the README you expect.
3. **Delete the other branch.** On the server, go to Repository ➜ Branches. Click the trash can next to the branch you want to delete.

### Connection Errors in WSL

If you're using WSL, you might get an error like this when you try to push changes to the server:

```
fatal: unable to access 'https://gitlab.com/jldowns-usna/ic322-computer-networks.git/': OpenSSL SSL_connect: Connection was reset in connection to gitlab.com:443
```

You may have a DNS issue in WSL. You can know that you have a DNS issue if `ping google.com` fails, but `ping 172.253.115.100` succeeds. To fix this, you need to change the DNS settings in WSL. You can do this by editing the file `/etc/resolv.conf` in WSL. You can do this with the following command:

```bash
sudo nano /etc/resolv.conf
```
(Be sure to use `sudo`! You can use `vim` if you prefer.)

Change the contents of the file to the following:

```
nameserver 10.1.74.10
nameserver 8.8.8.8
```
Save the file and exit.

One last step. By default, Linux overwrites `resolv.conf` on every reboot. To prevent this from happening, run the following command:

```
sudo tee /etc/wsl.conf > /dev/null << EOF
[network]
generateResolvConf=false
EOF
```

Now the command `ping google.com` should work and you should not get connection issues when pushing changes to the server.

### Certificate Errors in WSL

You might get an error that looks like this:
   
```
fatal: unable to access 'https://gitlab.com/jldowns-usna/ic322-computer-networks.git/': SSL certificate problem: unable to get local issuer certificate
```

The problem is that our firewall intercepts HTTPS traffic and replaces the certificate with its own certificate. This is a security feature, but it causes problems with `git`. To fix this, you need to install the school's CAs. [These directions](https://apt.cs.usna.edu/docs/ssl-system.html) will have you run the following commands:

```
sudo apt -y install curl
curl apt.cs.usna.edu/ssl/install-ssl-system.sh | bash
```

After you run these commands, you should be able to push changes to the server.