# IC322: Computer Networks

## Week 0: Getting Ready

* Assignment and Learning Goals
    * Set up portfolio
* Lectures
    * [17 August (Monday Schedule): Welcome to Class](/lectures/lecture01-intro/README.md)
    * 18 August: Orientation

## Week 1: The Internet

* Topics
* [Assignment and Learning Goals](/assignments/week01/)
* Lectures
    * 21 August
    * 24 August
    * ~~25 August: No Class~~

## Week 2: Application Layer: HTTP

* Topics
* [Assignment and Learning Goals](/assignments/week02/)
    * learn `netcat`
    * Hit API endpoints, build API endpoints
    * Build your own Web server
* Lectures
    * 28 August
    * 31 August
    * 1 September

## Week 3: Application Layer: DNS

* Topics
* [Assignment and Learning Goals](/assignments/week03/)
* Lectures
    * ~~4 September: No Class (Labor Day)~~
    * 7 September
    * 8 September (Welcome, parents!)

## Week 3.5: TLS?


## Week 4: Transport Layer

* Topics
* Assignment and Learning Goals
* Lectures
    * 11 September
    * 14 September
    * 15 September

## Week 5: Transport Layer

* Topics
* Assignment and Learning Goals
* Lectures
    * 18 September
    * 21 September
    * 22 September

## Week 6: Six Week Assessment

* Topics
* Assignment and Learning Goals
* Lectures
    * 25 September
    * 28 September
    * 29 September

## Week 7: Transport Layer

* Topics
* Assignment and Learning Goals
* Lectures
    * 2 October
    * 5 October
    * 6 October

## Week 8: Network Layer: Data Plane

* Topics
* Assignment and Learning Goals
* Lectures
    * ~~9 October: No Class~~
    * 10 October (Friday Schedule)
    * 12 October
    * 13 October

## Week 9: Network Layer: Control Plane

* Topics
* Assignment and Learning Goals
* Lectures
    * 16 October
    * 19 October
    * 20 October

## Week 10: Link Layer

* Topics
* Assignment and Learning Goals
* Lectures
    * 23 October
    * 26 October
    * 27 October

## Week 11: Twelve Week Assessment

* Topics
* Assignment and Learning Goals
* Lectures
    * 30 October
    * 2 November
    * 3 November

## Week 12: Wireless and Mobile Networks

* Topics
* Assignment and Learning Goals
* Lectures
    * 6 November
    * 9 November
    * ~~10 November: No Class (Veterans Day)~~

## Week 13: Wireless and Mobile Networks

* Topics
* Assignment and Learning Goals
* Lectures
    * 13 November
    * 16 November
    * 17 November

## Week 14: Security in Computer Networks

* Topics
* Assignment and Learning Goals
* Lectures
    * 20 November
    * 22 November (Friday Schedule)
    * ~~23 November: No School (Thanksgiving)~~
    * ~~24 November: No School (Thanksgiving)~~

## Week 15: Security in Computer Networks

* Topics
* Assignment and Learning Goals
* Lectures
    * 27 November
    * 30 November
    * 1 December

## Week 16: Final Assessment

* Topics
* Assignment and Learning Goals
* Lectures
    * 4 December
    * 7 December
