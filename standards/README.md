# Standards for IC322

## Eat Your Vegetables

1. Every week there will be textbook problems assigned. Include the questions and answers in your Portfolio.

2. Every assignment has Learning Goals listed at the top. Answer *all* of the Learning Goals and include the answers in your Portfolio. Good answers take time! This is the most important task you will do every week. Plan on spending at least 2-3 hours answering the Learning Goals well.

**🤖 LLM Guidance**: This is one of the assignments that you should really do yourself. [Memory is the residue of thought](https://www.goodreads.com/quotes/4051309-memory-is-the-residue-of-thought). You should spend time thinking about and struggling through the Learning Goals and the textbook questions. If you're on a "further research" mission, then LLMs can help guide your research. But answer the questions *without* help first.

## Grow More Vegetables

These assignments ask you to contribute to our class Quizziz question bank. This can include writing new questions or editing/improving existing ones. These are the questions that we use to practice at the beginning of class every Thursday and Friday so please try your best to make them thoughtful, important questions! Our [Class Bulletin Board](https://docs.google.com/document/d/1M0ZK6oFZVgWBMS4ZgqY_FCec8o2WSLQ7LBkrCxC2IXE/edit?usp=sharing) has information about logging into our class account.

## Lab Assignments

Lab assignments contain instructions to complete a task, with questions to answer along the way. As with most of your Portfolio entries, write using complete sentences and complete ideas. Pay attention to formatting and how your Portfolio looks rendered in HTML.

When adding a Lab Assignment to your Portfolio, always include:

* A short **introduction** that describes at a high level what the lab was about, and what you learned while doing it. Even though this section comes first, you should write this section *after* you've completed the lab.
* A "**Collaboration**" and/or "Citation" section that includes all the people you worked with (and a short description of their impact on your assignment), any Chatbots you talked to (and a link to your conversations), and websites you referenced during the lab. If you provided website links thoughout your report you can simply include something like "Citation links included in report."
* A "**Process**" section. At a minimum you should link to the instructions you followed to complete the lab. If you deviated from the instructions, added your own explorations, or didn't finish, include those details. Anyone should be able to replicate your work using the information you include and link to in this section.
* The **questions** from the lab, and your answers.

**🤖 LLM Opportunity**: If you ever get stuck on an assignment, you can always ask an LLM to help you get unstuck. Try feeding in the lab instructions, your input, and the error message. If you don't fully understand *why* you're doing something in the lab, you can ask an LLM this, too! You can also ask your instructor these things, but you all tend to work on your assignments when I tend to be sleeping.

## Programming Assignments

Some Lab assignments will be involve making a program. These assignments should always have a README.md that describes how to run your program. It makes sense to include a *Running the Program* section in your Portfolio's lab report.

If there is an installation step, your report must include those instructions. I much prefer for programs to be written in Python 3 and to run with a simple `python3 your-program.py` command (of course with your actual program name.)

For complicated programs with multiple dependencies, I'd appreciate if you dockerize your program and include a `docker run` command that I can run in one line.

For programming assignments that run on special hardware (like Raspberry Pis), I'll ask you to include detailed installation and running instructions, and then also to run a demonstration in class. I'll provide feedback on both the report you include in your Portfolio as well as the demonstration.

In any case, a typical IC322 student should be able to replicate your setup following your lab instructions.

## Deep Dives

Most weeks you will have a choice to "dive deep" into one of the week's topics. This aligns with the course goal of "broad awareness of all topics, and a deeper understanding of specific topics."

Choose **one** of the Learning Goals and complete **one** of the following tasks:

1. **Independent Research**. Research an "aspect" of the Learning Goal and write around 1000 words. "As aspect" is pretty loose here: if a Learning Goal is "I can compare different physical technologies used in access networks, including dial-up, DSL, cable, fiber, and wireless", you might write a short report on how ISPs use right-of-way grants to install their fiber cables without the consent of the property owners. Or you could research coaxial cable technology and explain how it works. The intention of this assignment is to learn something not covered in the textbook. Try to write something you think is interesting, even if it doesn't *exactly* answer the question. Word count is not the primary metric of these "Dive Deep" assignments so don't stress if you go over (or slightly under).
2. **Instructional Content**. Create instructional content; something that can explain the topic to your classmates or future students. Instructional videos are especially valuable since the book already does a good job explaining the concepts in writing!. [Here's an example of a video that I made for Chapter 6](https://drive.google.com/file/d/1Lt3PhIgSCVQ0Lad5lCkSS46Q8ggII3Vy/view?usp=sharing). But the medium is up to you. Diagrams, collages, and dance routines are all fair game.

In either case, plan on adding your solution to the [Class Portfolio](https://docs.google.com/document/d/1Vh1JmKcLoR4OkEgo1j1SXhFT5wYdpadQTUEbvI1zBgw/edit?usp=sharing).

**🤖 LLM Opportunity**: After you write a research report, ask an AI to ask you questions about it. You can strengthen your report by answering those questions (if you think they're good questions.) You can also use the feedback to highlight areas that need clarification. Here is a prompt that I use:
> You are StudentBot (beep boop!). You are a student and I am a teacher. I will attempt to teach you a topic, and you ask questions if you don't understand something. At the end of the exercise you should tell me how well I did at explaining the topic and let me know if I made any errors or how I can improve. Sound good?

## Knowledge Checks

You will have one opportunity every week to take closed-book Knowledge Check(s). You can take that week's Check as well as retake any previous Check. You can also schedule EI to retake Knowledge Checks outside of class hours. Knowledge checks are intended to measure real learning - are you able to actually explain a concept without copying it down?

Knowledge Checks consist of answering the Learning Goals. You already answered these as part of your [Eat Your Vegetables](#eat-your-vegetables) assignments. Since Knowledge Checks are closed book, your answers will probably have less detail compared to your EYV answers. However, they need to demonstrate mastery of the subject.

<!-- For examples of good Knowledge Check answers compared to EYV answers, look at my [sample Portfolio](https://gitlab.com/jldowns-usna/ic322-downs-portfolio/-/tree/main/week01). -->

I will provide feedback for your Knowledge Checks as well as a grade for each question. Questions will be graded Meets Standards/Needs Revision.

## Exams

We will have six week, twelve week, and final exams. Each of these exams will consist of answering a random sample of Knowledge Checks. Just like the Knowledge Checks, questions are graded Meet Standards/Needs Revision. Unlike the Knowledge Checks, you cannot retake an exam.